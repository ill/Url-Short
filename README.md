# UrlShort
### To make a tiny URL.

This is a PHP program that can make a url shorter.

#### Requirements:

* PHP 5.1 or higher.
* R/W file access.
* HTTP url rewrite.

#### Deploy:

0. Upload all files to your PHP web server.
0. Enable rewrite function in your server.
0. Now your server may be available.

##### Rewrite rules:

For apache it may works just fine with the &lt;.htaccess&gt; file, it's contents:

	RewriteEngine on
	RewriteBase /
	RewriteRule ^([0-9a-z]+)$ index.php?go&a=$1
	RewriteRule ^:([0-9a-z]+)$ index.php?go&a=$1&method=javascript
	RewriteRule ^-([0-9a-z]+)$ index.php?go&a=$1&method=html
	RewriteRule ^_([0-9a-z]+)$ index.php?go&a=$1&method=click

For lighthttpd, you need to edit your &lt;lighttpd.conf&gt; file:

	url.rewrite = {
		"^([0-9a-z]+)$" => "index.php?go&a=$1",
		"^:([0-9a-z]+)$" => "index.php?go&a=$1&method=javascript",
		"^-([0-9a-z]+)$" => "index.php?go&a=$1&method=html",
		"^_([0-9a-z]+)$" => "index.php?go&a=$1&method=click"
	}

For nginx, you need to edit your &lt;nginx.conf&gt; file:

	location /{
		rewrite ^([0-9a-z]+)$ index.php?go&a=$1 last;
		rewrite ^:([0-9a-z]+)$ index.php?go&a=$1&method=javascript last;
		rewrite ^-([0-9a-z]+)$ index.php?go&a=$1&method=html last;
		rewrite ^_([0-9a-z]+)$ index.php?go&a=$1&method=click last;
	}

_Please see wiki if there's any trouble._